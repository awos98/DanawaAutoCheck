from selenium.webdriver import Keys

from login import *
from cal_time import today_timestamp

# 로그인 후 해당 모바일 다나와 출석체크 페이지로 이동
browser.switch_to.window(browser.window_handles[0])
browser.get('https://dpg.danawa.com/mobile/attendance/main')
time.sleep(5)

# 출석 캘린더 부분 a 태그 내용 가져오기
check_a_tag = browser.find_element(By.CLASS_NAME, "check-cal__btn").get_attribute("outerHTML")
print("check_a_tag : " + check_a_tag)

# 출석 체크 캘린더가 닫혀 있을 때
if "check-cal__btn--select" in check_a_tag:
    browser.find_element(By.CLASS_NAME, "check-cal__btn--select").send_keys(Keys.ENTER)
    browser.find_element(By.ID, f"danawa-dpg-mobile-attendance-main-calendar-button-day-{today_timestamp()}") \
        .send_keys(Keys.ENTER)
# 출석 체크 캘린더가 열려 있을 때
else:
    browser.find_element(By.ID, f"danawa-dpg-mobile-attendance-main-calendar-button-day-{today_timestamp()}") \
        .send_keys(Keys.ENTER)
time.sleep(5)
