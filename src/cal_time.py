from datetime import date, datetime
import time


def today_timestamp():
    curr_date = f"{date.today()} 0:0:0"
    curr_date_timestamp = time.mktime(datetime.strptime(curr_date, '%Y-%m-%d %H:%M:%S').timetuple())
    dwn_timestamp = int(curr_date_timestamp) * 1000
    return dwn_timestamp
