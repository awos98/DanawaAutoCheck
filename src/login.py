from selenium.webdriver.common.by import By
import time

from settings.set_chrome_driver import set_chrome_driver_mb
from settings.base import KAKAO_ID, KAKAO_PW

# 다나와 로그인 페이지 접속
browser = set_chrome_driver_mb()
browser.get('https://auth.danawa.com/login?url=http%3A%2F%2Fwww.danawa.com%2Fmember%2FmyPage.php')
browser.implicitly_wait(3)

# 카카오 로그인 클릭
browser.find_element(By.ID, "danawa-member-login-snsButton-kakao").click()

# 새 탭(카카오 로그인) 페이지 전환
browser.switch_to.window(browser.window_handles[-1])

browser.get(browser.current_url)

kakao_id = browser.find_element(By.ID, "loginKey--1")
kakao_pw = browser.find_element(By.ID, "password--2")

# 카카오 계정 id, pw 입력
kakao_id.send_keys(KAKAO_ID)
kakao_pw.send_keys(KAKAO_PW)

# 로그인 버튼 클릭
browser.find_element(By.CLASS_NAME, "btn_g").click()
time.sleep(5)
