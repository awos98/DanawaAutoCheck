from dotenv import load_dotenv
import os

load_dotenv()

KAKAO_ID = os.environ.get("EMAIL")
KAKAO_PW = os.environ.get("PW")
